# 🚧 create a book

# Refs

- [My (old) previus blog post about asciidoctor and GitLab CI](https://medium.com/@k33g_org/cr%C3%A9er-une-cha%C3%AEne-de-publication-de-book-s-avec-gitlab-gitlab-ci-vagrant-asciidoctor-46e6f7ef7e71)


# Fonts

- OpenSans: http://www.1001fonts.com/open-sans-font.html
- SourceCodePro: http://www.1001fonts.com/source-code-pro-font.html
- EmojiOne: https://github.com/GuangchuangYu/emojifont/blob/master/inst/emoji_fonts/EmojiOne.ttf
- mplus1mn-regular-ascii-conums.ttf 🤔
