== Chapitre 1: 1er contact

NOTE: An admonition paragraph draws the reader's attention to
auxiliary information.
Its purpose is determined by the label
at the beginning of the paragraph.

<<<

=== You can use emojiis

Hello 🌍 👋

=== And add some source code

[source,javascript]
----
//hello world // <1>

function hello() { // <2>  
  let panda = `🐼`
  return '2 // <3>
} 

----
<1> this is a comment
<2> _this is a function_
<3> *return value*
